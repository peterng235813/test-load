# Note

## Architect

- This is a project sample, follow `clean architect`. This API flow has 3 tier: transport(controller), use-case(business/biz/service ), entity(store/repository)
- Transport: input request
- Biz: implement logic, use-case
- Repository: call to DB: save, query

## CLI
- nest new store-management
- nest generate mo users /modules
- nest g co users /modules
- nest g s users /modules

## Env
- node version: 18 lts
- yarn version: 3.5 stable
